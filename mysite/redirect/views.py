import requests, json
from requests.exceptions import HTTPError
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import RedirectView
from django.conf.urls import url
from django.shortcuts import redirect

def index(request):
    APIKey = "RGAPI-85342eab-abde-42b7-a356-b227040f19d2"
    summonerName = "yamchah2"
    idJSON = requestSummonerData(summonerName, APIKey)
    summonerID = idJSON['id']
    summonerID = str(summonerID)
    print(summonerID)
    matchJSON = requestMatchData(summonerID, APIKey)
    print(matchJSON['participants'][9])
    x = getMyX(summonerID, APIKey)
    print(x)
    champID =  matchJSON['participants'][x]['championId']
    championName = getChampNameByID(champID)
    championName = championName.lower()
    print(championName)
    if "'" in championName:
        championName.replace('\'', '')
    
    url = "http://champion.gg/champion/" + championName + "/"
    return redirect(url)

#urlpatterns = [
#    url(r'^one/$', RedirectView.as_view(url='www.google.co.za')),
#]

def getMyX(summonerID, APIKey):
    myJ = requestMatchData(summonerID, APIKey)
    for x in range(0, 10):
        id = myJ['participants'][x]['summonerId']
        if str(id) == summonerID:
            return x

def getChampNameByID(championID):
    with open("champions.json") as champs:
        champJSON = json.load(champs)
    for key, value in champJSON.items():
        for key, value in value.items():
            if value["id"] == championID:
                return str(key)

def requestSummonerData(summonerName, APIKey):
    URL = "https://euw1.api.riotgames.com/lol/summoner/v3/summoners/by-name/" + summonerName + "?api_key=" + APIKey
    response = requests.get(URL)
    return response.json()

def requestMatchData(summonerID, APIKey):
    URL = "https://euw1.api.riotgames.com/lol/spectator/v3/active-games/by-summoner/" + summonerID + "?api_key=" + APIKey
    response = requests.get(URL)
    return response.json()
    
# Create your views here.
